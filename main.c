#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#include <arpa/inet.h>
#include <netinet/if_ether.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <pcap.h>

#include "hist.h"
#include "usage.h"
#include "tcp.h"

long total_data = 0;
long start_time = 0;
long end_time = 0;

int main(int argc, char **argv) {
  // Print usage if no arguments.
  if (argc < 3) {
    print_usage();
    return 0;
  }

  // Initialize pcap with input file.
  const char *pcap_filename = argv[1];
  printf("Opening file: \"%s\" ... ", pcap_filename);
  char error[PCAP_ERRBUF_SIZE];
  pcap_t *pcap;
  pcap = pcap_open_offline(argv[1], error);
  // Print error if bad file read.
  if (pcap == NULL) {
    printf("\nERROR reading pcap file: %s\n", error);
    return 1;
  } else {
    printf("DONE\n");
  }

  init_proto_hist();
  int tcpprint_index = -1;
  if (strcmp(argv[2], "tcpprint") == 0 || strcmp(argv[2], "tcpstream") == 0) {
    if (argc < 4) {
      printf("ERROR!!!: tcp stream index needed for command\n");
      print_usage();
      return 1;
    }
    tcpprint_index = atoi(argv[3]);
    printf("Printing tcp stream index %i\n", tcpprint_index);
  }

  // Loop through packets.
  const unsigned char *packet;
  unsigned int packet_len;
  struct pcap_pkthdr header;
  while ((packet = pcap_next(pcap, &header)) != NULL){
    // Retreive basic packet information.
    packet_len = header.len;

    // Error checking
    if (header.len != header.caplen) {
      // packet was split.
      break;
    }

    // total bandwidth metrics
    total_data += header.len;
    if (start_time == 0) {
      start_time = header.ts.tv_sec;
    }
    end_time = header.ts.tv_sec;

    // Get Ethernet header
    if (packet_len < sizeof(struct ether_header)) {
      printf("ERROR: Cut before ethernet header.");
      continue;
    }
    struct ether_header *header_ethernet = (struct ether_header*)packet;
    packet_len -= sizeof(struct ether_header);
    packet += sizeof(struct ether_header);
    if (ntohs(header_ethernet->ether_type) != 0x0800) {
      printf("  ERROR: Unsupported ethernet type\n");
      continue;
    }

    // Get IP Header.
    if (packet_len < sizeof(struct ip)) {
      printf("ERROR: Cut before IP header.\n");
      continue;
    }
    struct ip *header_ip = (struct ip*)packet;
    packet_len -= sizeof(struct ip);
    packet += sizeof(struct ip);
    if (header_ip->ip_v != 4) {
      // IP version is not 4
      printf("  ERROR: UNSUPPORTED IP VERSION\n");
      continue;
    }

    if (header_ip->ip_p < proto_hist_len) {
      proto_hist[header_ip->ip_p].packet_count++;
      proto_hist[header_ip->ip_p].total_data +=
          header_ip->ip_len - sizeof(struct ip);
    } else {
      printf("  ERROR: Unsupported Protocol\n");
    }

    if (header_ip->ip_p != 6) {
      //continue without tcp processing
      continue;
    }

    // Get TCP header.
    if (packet_len < sizeof(struct tcphdr)) {
      printf("  ERROR: Cut before tcp header.");
      continue;
    }
    struct tcphdr *header_tcp = (struct tcphdr*)packet;
    packet_len -= header_tcp->th_off * 4;
    packet += header_tcp->th_off * 4;

    tcp_connection curr_con;
    curr_con.src_port = ntohs(header_tcp->th_sport);
    curr_con.dst_port = ntohs(header_tcp->th_dport);
    curr_con.src_ip = header_ip->ip_src;
    curr_con.dst_ip = header_ip->ip_dst;

    if (header_tcp->th_flags == TH_SYN && tcp_connections_len < MAX_TCP) {
      // New TCP connection
      tcp_connections[tcp_connections_len] = curr_con;
      tcp_connections_len++;
    }
    int curr_con_index = get_tcp_connection_index(&curr_con);
    int curr_con_dir = get_tcp_connection_direction(&curr_con);

    unsigned int tcp_payload_size =
        ntohs(header_ip->ip_len) - sizeof(struct ip) - header_tcp->th_off * 4;

    handle_tcp_packet(header_tcp, header_ip, packet, tcp_payload_size);

    if (tcpprint_index != -1 && curr_con_index == tcpprint_index && 
        strcmp(argv[2], "tcpprint") == 0) {
      if (curr_con_dir == TCP_OUT) {
        printf("  Packet Outgoing ");
      } else {
        printf("  Packet Incoming ");
      }
      printf("seq=%u ", ntohl(header_tcp->th_seq));
      if (header_tcp->th_flags & TH_FIN)  { printf("FIN ");  }
      if (header_tcp->th_flags & TH_SYN)  { printf("SYN ");  }
      if (header_tcp->th_flags & TH_RST)  { printf("RST ");  }
      if (header_tcp->th_flags & TH_PUSH) { printf("PUSH "); }
      if (header_tcp->th_flags & TH_URG)  { printf("URG ");  }
      if (header_tcp->th_flags & TH_ACK)  {
        printf("ACK=%u ", ntohl(header_tcp->th_ack));
      }
      printf("Payload size: %i bytes\n", tcp_payload_size);
      if (tcp_payload_size == 0) {
        printf("      (no payload)");
      } else {
        for (int i = 0; i < tcp_payload_size; i++) {
          if (i && i % 16 == 0) {
            printf("\n");
          }
          if (i % 16 == 0) {
            printf("      ");
          }
          printf("%02x ", *packet);
          packet_len--;
          packet++;
        }
      }
      printf("\n");
    }
  }

  if (strcmp(argv[2], "histogram") == 0) {
    print_proto_hist();
  } else if (strcmp(argv[2], "bandwidth") == 0) {
    printf("\nTOTAL AVERAGE BANDWIDTH: %f bytes/sec\n", 
           (float)total_data/(end_time-start_time));
  } else if (strcmp(argv[2], "tcplist") == 0) {
    printf("\nTCP CONNECTION COUNT: %li\n", tcp_connections_len);
    for (int i = 0; i < tcp_connections_len; i++) {

      char srcip[INET_ADDRSTRLEN];
      char dstip[INET_ADDRSTRLEN];
      inet_ntop(AF_INET, &(tcp_connections[i].src_ip), srcip, INET_ADDRSTRLEN);
      inet_ntop(AF_INET, &(tcp_connections[i].dst_ip), dstip, INET_ADDRSTRLEN);

      printf("  Connection #%i \t %s:%i => %s:%i \t\n", i, srcip,
        tcp_connections[i].src_port, dstip, tcp_connections[i].dst_port);
    }
  }

  if (tcpprint_index != -1 && strcmp(argv[2], "tcpstream") == 0) {
    extract_tcp_bytestream(&tcp_connections[tcpprint_index]);
  }

  free_packet_list();

  return 0;
}
