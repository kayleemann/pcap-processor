#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#include <arpa/inet.h>
#include <netinet/if_ether.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <pcap.h>

typedef struct {
  struct in_addr src_ip, dst_ip;
  u_short src_port, dst_port;
} tcp_connection;

typedef struct tcp_packet {
  struct tcphdr header;
  tcp_connection connection;
  size_t payload_size;
  u_char *payload;
  struct tcp_packet *next;
} tcp_packet;

extern tcp_packet *packet_list;
extern size_t packet_list_len;

static const size_t MAX_TCP = 1024;  // Max number of TCP Connections to list.
extern tcp_connection tcp_connections[MAX_TCP];
extern size_t tcp_connections_len;

static const int TCP_OUT = 1;
static const int TCP_IN  = 2;

void extract_tcp_bytestream(const tcp_connection *connection);

void handle_tcp_packet(const struct tcphdr *header, const struct ip *header_ip,
	                   const unsigned char *data, size_t len);

int tcp_connection_match(const tcp_connection *a, const tcp_connection *b);

int get_tcp_connection_index(const tcp_connection *connection);

int get_tcp_connection_direction(const tcp_connection *connection);

// Must be called if 'handle_tcp_packet' is used;
void free_packet_list();

