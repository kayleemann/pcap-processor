# pcap-processor

by Kaylee Linnea Mann all Rights Reserved

## Usage:
 `pcap-processor <pcap-input-file> <operation>`

where `<pcap-input-file>` contains the input data in pcap format

where `<operation>` is one of the below operations

## Operations:

* **histogram**: Display a distogram of the ammount of packets and data for
             each protocol.
* **bandwidth**: print the total bandwidth over the capture period.
* **tcplist**  : list the tcp connections established.
* **tcpprint** : Print out of order tcp packets for tcp connection at the
             index privided. 
             In this case you must introduce an additional command line
             param which is the stream index as listed in tcplist.
* **tcpstream**: Similar to tcpprint but only prints the in-order
             bytestream.

## Example

Note: New .pcap files can be generated with a command of the form: `sudo tcpdump -i en0 -w example.pcap`

`pcap-processor example.pcap histogram`

`pcap-processor example.pcap tcpprint 5`

`pcap-processor example.pcap tcpstream 5`
