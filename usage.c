#include <stdio.h>
#include "usage.h"

static const char *USAGE_MESSAGE = \
  "PCAP PROCESSOR\n"
  "  by Kaylee Linnea Mann all Rights Reserved\n"
  "\n"
  "Usage:\n"
  "  pcap-processor <pcap-input-file> <operation> \n"
  "    where <pcap-input-file> contains the input data in pcap format\n"
  "    where <operation> is one of the below operations\n"
  "\n"
  "Operations:\n"
  "  'histogram': Display a distogram of the ammount of packets and data for\n"
  "               each protocol.\n"
  "  'bandwidth': print the total bandwidth over the capture period.\n"
  "  'tcplist'  : list the tcp connections established.\n"
  "  'tcpprint' : Print out of order tcp packets for tcp connection at the\n"
  "               index privided. \n"
  "               In this case you must introduce an additional command line\n"
  "               param which is the stream index as listed in tcplist.\n"
  "  'tcpstream': Similar to tcpprint but only prints the in-order\n"
  "               bytestream.\n";

void print_usage() {
  printf("%s", USAGE_MESSAGE);
}
