#include "hist.h"

proto_hist_entry proto_hist[proto_hist_len];

static const size_t named_protos_len = 18;
static const char *named_protos[named_protos_len] = {
  "HOPOPT", "ICMP  ", "IGMP  ", "GGP   ", "IPi...", "ST    ", "TCP   ", "CBT   ", "EGP   ", "IGP   ",
  "BBN...", "NVP-II", "PUP   ", "ARGUS ", "EMCON ", "XNET  ", "CHAOS ", "UDP   "};

void init_proto_hist() {
  for (int i = 0; i < proto_hist_len; i++) {
    proto_hist[i].name[0] = '\0';
    proto_hist[i].packet_count = 0;
    proto_hist[i].total_data = 0;
  }
  for (int i = 0; i < named_protos_len; i++) {
    strcpy(proto_hist[i].name, named_protos[i]);
  }
}

void print_proto_hist() {
  /*
  printf("\nPROTOCOL HISTOGRAM TABLE: \n");
  for (int i = 0; i < proto_hist_len; i++) {
    if (proto_hist[i].packet_count == 0) {
      continue;
    }
    printf("  %s(#%i) \t %li packets \t %li bytes\n", 
      proto_hist[i].name, i, proto_hist[i].packet_count, proto_hist[i].total_data);
  }
  */
  print_proto_graph(0);
  print_proto_graph(1);
}

void print_proto_graph(int mode) {
  printf("\nPROTOCOL HISTOGRAM GRAPH %s: \n", 
         mode ? "(DATA TRANSMITTED)" : "(PACKETS SENT)");
  long max_packet_count = 5;
  for (int i = 0; i < proto_hist_len; i++) {
    long this_count = mode ? proto_hist[i].total_data : proto_hist[i].packet_count;
    if (this_count > max_packet_count) {
      max_packet_count = this_count;
    }
  }
  double packets_per_char = (double)max_packet_count / 50.0;
  printf("\u2554");
  for (int i = 0; i < 50 + 28; i++) { printf("\u2550"); }
  printf("\u2557\n");
  int colorindex = 0;
  for (int i = 0; i < proto_hist_len; i++) {
    if (proto_hist[i].packet_count == 0) {
      continue;
    }
    colorindex++;
    if (mode) {
      printf("\u2551 %s(#%2i)%13lib ", 
        proto_hist[i].name, i, proto_hist[i].total_data);
    } else {
      printf("\u2551 %s(#%2i)%6li packets ", 
        proto_hist[i].name, i, proto_hist[i].packet_count);
    }
    if (colorindex % 2 == 0) {
      printf("\033[31m"); // set color red.
    } else {
      printf("\033[34m"); // set color red.
    }
    long bar_len;
    if (mode) {
      bar_len = (long)(proto_hist[i].total_data / packets_per_char);
    } else {
      bar_len = (long)(proto_hist[i].packet_count / packets_per_char);
    }
    for (int i = 0; i < bar_len; i++) {
      printf("\u2588");
    }
    for (int i = bar_len; i < 50; i++) {
      printf(" ");
    }
    printf("\033[0m"); // reset color.
    printf(" \u2551\n");
  }
  printf("\u255A");
  for (int i = 0; i < 50 + 28; i++) { printf("\u2550"); }
  printf("\u255D\n");
}
