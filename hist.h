#include <stdio.h>
#include <time.h>
#include <string.h>

#include <arpa/inet.h>
#include <netinet/if_ether.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <pcap.h>

typedef struct {
  char name[32];
  long packet_count;
  long total_data;
} proto_hist_entry;

static const size_t proto_hist_len = 142;
extern proto_hist_entry proto_hist[proto_hist_len];

void print_proto_hist();

void init_proto_hist();

void print_proto_graph();
