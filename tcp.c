#include <ctype.h>
#include "tcp.h"

tcp_connection tcp_connections[MAX_TCP];
size_t tcp_connections_len = 0;

tcp_packet *packet_list = 0;
size_t packet_list_len = 0;
size_t packet_list_cap = -1;

void handle_tcp_packet(const struct tcphdr *header, const struct ip *header_ip,
                       const unsigned char *data, size_t len) {
  if (packet_list == 0) {
    packet_list = malloc(sizeof(tcp_packet) * 100);
    packet_list_cap = 100;
  }
  if (packet_list_len == packet_list_cap) {
    // grow packet array
    tcp_packet *prev_packet_list = packet_list;
    packet_list = malloc(sizeof(tcp_packet) * packet_list_cap * 2);
    memcpy(packet_list, prev_packet_list, sizeof(tcp_packet) * packet_list_cap);
    free(prev_packet_list);
    packet_list_cap *= 2;
  }
  packet_list[packet_list_len].header = *header;
  packet_list[packet_list_len].payload_size = len;
  packet_list[packet_list_len].payload = malloc(len);
  memcpy(packet_list[packet_list_len].payload, data, len);
  packet_list[packet_list_len].connection.src_ip = header_ip->ip_src;
  packet_list[packet_list_len].connection.dst_ip = header_ip->ip_dst;
  packet_list[packet_list_len].connection.src_port = ntohs(header->th_sport);
  packet_list[packet_list_len].connection.dst_port = ntohs(header->th_dport);
  packet_list[packet_list_len].next = 0;
  packet_list_len++;
}

void free_packet_list() {
  if (packet_list) {
    free(packet_list);
    packet_list = 0;
  }
}

void extract_tcp_bytestream(const tcp_connection *connection) {
  printf("Extracting bytestream\n");
  unsigned long src_seq_offset = 0;
  unsigned long dst_seq_offset = 0;
  unsigned char *out_buffer = 0;
  unsigned long out_buffer_len = 0;
  unsigned char *in_buffer = 0;
  unsigned long in_buffer_len = 0;
  for (int i = 0; i < packet_list_len; i++) {
    tcp_packet *packet = &packet_list[i];
    // Check if packet is part of connection.
    if (tcp_connection_match(connection, &packet->connection)) {
      // Get direction of packet.
      int dir = get_tcp_connection_direction(&packet->connection);
      if(packet->header.th_flags == TH_SYN) {
        src_seq_offset = ntohl(packet->header.th_seq) + 1;
        continue;
      } else if((packet->header.th_flags & TH_SYN) && 
                (packet->header.th_flags & TH_ACK)) {
        // initial syn packet found
        dst_seq_offset = ntohl(packet->header.th_seq) + 1;
        continue;
      } else if(packet->header.th_flags & TH_PUSH) {
        if (dir == TCP_OUT) {
          // Update buffer len.
          if(ntohl(packet->header.th_seq) - src_seq_offset +
              packet->payload_size > out_buffer_len) {
            out_buffer_len = ntohl(packet->header.th_seq) - 
                src_seq_offset + packet->payload_size;
            out_buffer = realloc(out_buffer, out_buffer_len);
          }
          // Copy payload.
          memcpy(out_buffer + (ntohl(packet->header.th_seq) - src_seq_offset), 
                 packet->payload, packet->payload_size);
        } else if (dir == TCP_IN) {
          // Update buffer len.
          if (ntohl(packet->header.th_seq) - dst_seq_offset + 
              packet->payload_size > in_buffer_len){
            in_buffer_len = ntohl(packet->header.th_seq) - 
                dst_seq_offset + packet->payload_size;

              printf("realloc %lu %lu\n", dst_seq_offset, in_buffer_len);

            in_buffer = realloc(in_buffer, in_buffer_len);
            if (in_buffer == 0) {
              printf("ERROR: OOM\n");
            }
          }
          // Copy payload.
          memcpy(in_buffer + (ntohl(packet->header.th_seq) - dst_seq_offset), 
                 packet->payload, packet->payload_size);
        }
      }
    }
  }
  printf("TCP bytestream HEX: \n");
  printf("  Outgoing bytes: ");
  for (int i = 0; i < out_buffer_len; i++) {
    if (i % 16 == 0) {
      printf("\n    ");
    }
    printf("%02x ", out_buffer[i]);
  }
  printf("\n");
  printf("  Incoming bytes: ");
  for (int i = 0; i < in_buffer_len; i++) {
    if (i % 16 == 0) {
      printf("\n    ");
    }
    printf("%02x ", in_buffer[i]);
  }
  printf("\n");
  printf("TCP bytestream ASCII: \n");
  printf("  Outgoing bytes: ");
  for (int i = 0; i < out_buffer_len; i++) {
    if (i % 32 == 0) {
      printf("\n    ");
    }
    if (isprint((char)out_buffer[i])){
      printf("%c", (char)out_buffer[i]);
    } else {
      printf(".");
    }
  }
  printf("\n");
  printf("  Incoming bytes: ");
  for (int i = 0; i < in_buffer_len; i++) {
    if (i % 32 == 0) {
      printf("\n    ");
    }
    if (isprint((char)in_buffer[i])){
      printf("%c", (char)in_buffer[i]);
    } else {
      printf(".");
    }
  }
  printf("\n");
  if (out_buffer) {
    free(out_buffer);
    out_buffer = 0;
  }
  if (in_buffer) {
    free(in_buffer);
    in_buffer = 0;
  }
}

int tcp_connection_match(const tcp_connection *a, const tcp_connection *b) {
  if (a->src_port == b->src_port &&
      a->dst_port == b->dst_port &&
      a->src_ip.s_addr == b->src_ip.s_addr &&
      a->dst_ip.s_addr == b->dst_ip.s_addr) {
    return TCP_OUT;
  } else if (a->src_port == b->dst_port &&
      a->dst_port == b->src_port &&
      a->src_ip.s_addr == b->dst_ip.s_addr &&
      a->dst_ip.s_addr == b->src_ip.s_addr) {
    return TCP_IN;
  } else {
    return 0;
  }
}

int get_tcp_connection_index(const tcp_connection *connection){
  for (int i = 0; i < tcp_connections_len; i++) {
    const tcp_connection *o_connection = &tcp_connections[i];
    if (tcp_connection_match(o_connection, connection)) {
      return i;
    }
  }
  return -1;
}

int get_tcp_connection_direction(const tcp_connection *connection){
  for (int i = 0; i < tcp_connections_len; i++) {
    const tcp_connection *o_connection = &tcp_connections[i];
    if (tcp_connection_match(o_connection, connection)) {
      return tcp_connection_match(o_connection, connection);
    }
  }
  return 0;
}
