CC=gcc
CFLAGS=
DEPS=hist.h usage.h tcp.h
OBJ=main.o hist.o usage.o tcp.o

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

pcap-processor: $(OBJ)
	gcc -o $@ $^ $(CFLAGS) -lpcap

clean:
	rm -f $(OBJ) pcap-processor
